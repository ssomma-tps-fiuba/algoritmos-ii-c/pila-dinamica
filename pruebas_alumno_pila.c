#include "pila.h"
#include "testing.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#define VOLUMEN_PRUEBAS 10000


/* ******************************************************************
 *                   PRUEBAS UNITARIAS ALUMNO
 * *****************************************************************/

// Prueba de creacion de pila
void prueba_crear(){

	pila_t* pila = pila_crear();
	print_test("Pila creada", pila != NULL);
	pila_destruir(pila);   

}


// Prueba de vacio en pila recien creada
void prueba_creada_vacia(){

	pila_t* pila = pila_crear();
	print_test("Pila creada y vacia", pila != NULL && pila_esta_vacia(pila));
	pila_destruir(pila);

}


// Prueba de comportamiento de pila vacia
void pruebas_pila_vacia(){

	pila_t* pila = pila_crear();
	bool pila_vacia = pila_esta_vacia(pila);
	bool null_tope = !pila_ver_tope(pila);
	bool null_desapilar = !pila_desapilar(pila);
	print_test("Desapilar y tope invalidos en pila vacia", pila_vacia && null_tope && null_desapilar);
	pila_destruir(pila);

}


// Prueba de apilado de NULL
void prueba_apilar_NULL(){

	pila_t* pila = pila_crear();
	print_test("Puedo apilar un NULL y no va a estar vacia", pila_apilar(pila, NULL) && pila_ver_tope(pila) == NULL && !pila_esta_vacia(pila));
	pila_destruir(pila);

}


// Prueba de apilado de dato
void prueba_apilar(){

	pila_t* pila = pila_crear();
	int dato1 = 64;
	int dato2 = 128;
	int dato3 = 256;
	print_test("Apilo 3 datos", pila_apilar(pila, &dato1) && pila_apilar(pila, &dato2) && pila_apilar(pila, &dato3) && !pila_esta_vacia(pila));
	int* tope = pila_ver_tope(pila);
	print_test("El tope es igual al ultimo apilado", *tope == dato3 && tope == &dato3);
	pila_destruir(pila);

}


// Prueba de desapilado de datos
void prueba_desapilar(){

	pila_t* pila = pila_crear();
	int dato = 256;
	pila_apilar(pila, &dato);
	int* tope = pila_ver_tope(pila);
	int* desapilado = pila_desapilar(pila);
	print_test("El dato, el tope y el desapilado son iguales", &dato == tope && &dato == desapilado && pila_esta_vacia(pila));
	print_test("El valor del dato se mantuvo", *desapilado == dato);
	pila_destruir(pila);

}


void pruebas_volumen(){

	pila_t* pila = pila_crear();
	int datos[VOLUMEN_PRUEBAS];
	bool prueba_ok;
	
	for(int i = 0; i <= VOLUMEN_PRUEBAS; i++){
			
		prueba_ok = pila_apilar(pila, &datos[i]) && pila_ver_tope(pila) == &datos[i] && !pila_esta_vacia(pila); // Validacion de apilado

		if(!prueba_ok){
			break;
		}	    
	}

	print_test("Paso pruebas de volumen apilado", prueba_ok);
	
	for(int i = 0; i <= VOLUMEN_PRUEBAS; i++){
	
		int* tope = pila_ver_tope(pila);
		int* desapilado = pila_desapilar(pila);
		prueba_ok = tope == desapilado && desapilado == &datos[VOLUMEN_PRUEBAS - i] ; // Validacion de desapilado
		
		if(!prueba_ok){
			break;
		}	
	}

	print_test("Paso pruebas de volumen desapilado", prueba_ok);

	print_test("Mantiene comportamiento de pila vacia", !pila_ver_tope(pila) && !pila_desapilar(pila));
	
	pila_destruir(pila);   
}


void pruebas_pila_alumno() {

	pila_t* ejemplo = NULL;
	print_test("Puntero inicializado a NULL", ejemplo == NULL);

	prueba_crear();
	prueba_creada_vacia();
	pruebas_pila_vacia();
	prueba_apilar_NULL();
	prueba_apilar();
	prueba_desapilar();
	pruebas_volumen();
	
}
