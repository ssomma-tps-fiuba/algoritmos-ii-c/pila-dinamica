#include "pila.h"
#include <stdlib.h>

#define CAPACIDAD_BASE 2
#define FACTOR_REDIMENSION 2
#define CAPACIDAD_OCUPADA 2
#define CAPACIDAD_LIBRE 4

/* Definición del struct pila proporcionado por la cátedra.
 */
struct pila {
    void** datos;
    size_t cantidad;  // Cantidad de elementos almacenados.
    size_t capacidad;  // Capacidad del arreglo 'datos'.
};

/* *****************************************************************
 *                    PRIMITIVAS DE LA PILA
 * *****************************************************************/

pila_t* pila_crear(void){

    pila_t* pila = malloc(sizeof(pila_t));
    
    if(pila == NULL){
        return NULL;
    }

    pila->datos = malloc(sizeof(void*) * CAPACIDAD_BASE);

    if(pila->datos == NULL){

        free(pila);
        return NULL;

    }

    pila->cantidad = 0;
    pila->capacidad = CAPACIDAD_BASE;

    return pila;

}

void pila_destruir(pila_t* pila){

    free(pila->datos);
    free(pila);

}

// Redimensiona la cantidad de memoria asignada a una pila
// Recibe un puntero a pila y un bool que marca si debe agrandarse o disminuirse la cantidad de memoria asignada
// Pre: la pila fue creada
// Post: devuelve verdadero si se redimensiono con exito, falso en caso contrario 
bool pila_redimensionar(pila_t* pila, size_t capacidad_nueva){
    
    capacidad_nueva = (capacidad_nueva == 0) ? 1 : capacidad_nueva;

    void** datos_nuevo = realloc(pila->datos, capacidad_nueva * sizeof(void*));

    if(capacidad_nueva > 0 && datos_nuevo == NULL){
        return false;
    }
    
    pila->datos = datos_nuevo;
    pila->capacidad = capacidad_nueva;

    return true;
}


bool pila_esta_vacia(const pila_t* pila){

   return pila->cantidad == 0;
    
}

void* pila_ver_tope(const pila_t* pila){

    if(pila_esta_vacia(pila)){
        return NULL;
    }

    return pila->datos[pila->cantidad - 1];
    
}

bool pila_apilar(pila_t* pila, void* valor){

    size_t cant_aux = pila->cantidad + 1;
    
    if(cant_aux * CAPACIDAD_OCUPADA >= pila->capacidad){
       
        if(!pila_redimensionar(pila, pila->capacidad * FACTOR_REDIMENSION)){
            return false;
        }
    }

    pila->datos[pila->cantidad] = valor;
    pila->cantidad = cant_aux; 

    return true;
}


void* pila_desapilar(pila_t* pila){

    if(pila_esta_vacia(pila)){
        return NULL;
    }

    void* desapilado = pila->datos[pila->cantidad - 1];
    pila->cantidad--;

    if(pila->cantidad * CAPACIDAD_LIBRE <= pila->capacidad){
        pila_redimensionar(pila, pila->capacidad / FACTOR_REDIMENSION);
    }

    pila->datos[pila->cantidad] = NULL;
    
    return desapilado;

}